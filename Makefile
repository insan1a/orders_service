.PHONY: run
run: build
	./bin/orders_service

.PHONY: build
build:
	go build -o ./bin/orders_service ./cmd/main.go

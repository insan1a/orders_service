module gitlab.com/insan1a/orders_service

go 1.21.5

require (
	github.com/gorilla/mux v1.8.1
	github.com/sirupsen/logrus v1.9.3
	github.com/tarantool/go-tarantool v1.12.2
	golang.org/x/sync v0.6.0
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/mattn/go-pointer v0.0.1 // indirect
	github.com/spacemonkeygo/spacelog v0.0.0-20180420211403-2296661a0572 // indirect
	github.com/tarantool/go-openssl v0.0.8-0.20231004103608-336ca939d2ca // indirect
	github.com/vmihailenco/msgpack/v5 v5.4.1 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
	google.golang.org/appengine v1.6.8 // indirect
	google.golang.org/protobuf v1.32.0 // indirect
	gopkg.in/vmihailenco/msgpack.v2 v2.9.2 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

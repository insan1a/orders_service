package errs

import (
	"errors"
	"reflect"
)

var ErrInvalidErrProvided = errors.New("the provided error is not a Tarantool error")

func ParseTarantoolError(err map[any]any) error {
	if !validateRequiredTarantoolErrorFields(err) {
		return ErrInvalidErrProvided
	}

	return errors.New(err["message"].(string))
}

func validateRequiredTarantoolErrorFields(err map[any]any) bool {
	var requiredFields = map[string]reflect.Kind{
		"code":  reflect.Uint64,
		"error": reflect.String,
	}

	for field, kind := range requiredFields {
		val, ok := err[field]
		if !ok {
			return false
		}

		if reflect.TypeOf(val).Kind() != kind {
			return false
		}
	}

	return true
}

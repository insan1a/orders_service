package tarantool

import (
	"time"
)

type Config struct {
	addr              string
	user              string
	password          string
	connectionTimeout time.Duration
}

// NewConfig creates a new tarantool connection config with provided address.
//
// The default values for user is 'guest', password is empty string,
// connectionTimeout is 5 seconds.
func NewConfig(addr string) Config {
	return Config{
		addr:              addr,
		user:              "guest",
		connectionTimeout: 5 * time.Second,
	}
}

func (c Config) WithUser(user string) Config {
	c.user = user
	return c
}

func (c Config) WithPassword(password string) Config {
	c.password = password
	return c
}

func (c Config) WithConnectionTimeout(timeout time.Duration) Config {
	c.connectionTimeout = timeout
	return c
}

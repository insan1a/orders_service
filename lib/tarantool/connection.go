package tarantool

import (
	"github.com/tarantool/go-tarantool"
)

type Conn struct {
	*tarantool.Connection
}

func New(cfg Config) (*Conn, error) {
	c, err := tarantool.Connect(cfg.addr, tarantool.Opts{
		Reconnect:     cfg.connectionTimeout,
		MaxReconnects: 5,
		User:          cfg.user,
		Pass:          cfg.password,
	})
	if err != nil {
		return nil, err
	}

	return &Conn{c}, nil
}

// Example of calling Lua function from Go
//
//	resp, err := tarantoolConn.Call("star.api.user.create", []interface{}{
//		map[string]any{
//			"email": "user@test.com",
//		},
//	})

package gracefulgroup

import (
	"context"
	"errors"
	"os/signal"

	"golang.org/x/sync/errgroup"
)

func New(cfg Config) (*errgroup.Group, context.Context) {
	ctx, cancel := signal.NotifyContext(context.Background(), cfg.signals...)

	group, groupCtx := errgroup.WithContext(ctx)
	group.Go(func() error {
		<-groupCtx.Done()
		cancel()
		if err := groupCtx.Err(); !errors.Is(err, context.Canceled) {
			return err
		}
		return nil
	})

	return group, groupCtx
}

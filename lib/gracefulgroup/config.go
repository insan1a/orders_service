package gracefulgroup

import "os"

type Config struct {
	signals []os.Signal
}

func NewConfig() Config {
	return Config{
		signals: []os.Signal{os.Interrupt},
	}
}

func (c Config) WithSignals(signals ...os.Signal) Config {
	c.signals = signals
	return c
}

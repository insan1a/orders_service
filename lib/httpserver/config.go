package httpserver

import (
	"net/http"
	"time"
)

type Config struct {
	addr         string
	readTimeout  time.Duration
	writeTimeout time.Duration
	idleTimeout  time.Duration
	handler      http.Handler
}

func NewConfig(addr string, handler http.Handler) Config {
	return Config{
		addr:         addr,
		readTimeout:  5 * time.Second,
		writeTimeout: 5 * time.Second,
		idleTimeout:  time.Minute,
		handler:      handler,
	}
}

func (c Config) WithReadTimeout(timeout time.Duration) Config {
	c.readTimeout = timeout
	return c
}

func (c Config) WithWriteTimeout(timeout time.Duration) Config {
	c.writeTimeout = timeout
	return c
}

func (c Config) WithIdleTimeout(timeout time.Duration) Config {
	c.idleTimeout = timeout
	return c
}

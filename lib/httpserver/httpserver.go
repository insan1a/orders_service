package httpserver

import (
	"context"
	"net/http"
)

type HTTPServer struct {
	httpserver http.Server
}

func New(cfg Config) *HTTPServer {
	return &HTTPServer{
		httpserver: http.Server{
			Addr:         cfg.addr,
			Handler:      cfg.handler,
			ReadTimeout:  cfg.readTimeout,
			WriteTimeout: cfg.writeTimeout,
			IdleTimeout:  cfg.idleTimeout,
		},
	}
}

func (s *HTTPServer) Start() error {
	return s.httpserver.ListenAndServe()
}

func (s *HTTPServer) Stop(ctx context.Context) error {
	return s.httpserver.Shutdown(ctx)
}

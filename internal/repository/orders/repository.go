package orders

import (
	"fmt"

	"github.com/sirupsen/logrus"
	gotarantool "github.com/tarantool/go-tarantool"

	"gitlab.com/insan1a/orders_service/internal/models"
	"gitlab.com/insan1a/orders_service/lib/errs"
	"gitlab.com/insan1a/orders_service/lib/tarantool"
)

type Repository struct {
	conn *tarantool.Conn
}

func New(conn *tarantool.Conn) *Repository {
	return &Repository{conn: conn}
}

func (r *Repository) Create(order *models.Order) error {
	if order == nil {
		return fmt.Errorf("try to create order: order is nil")
	}

	resp, err := r.conn.Call("star.api.order.create", []interface{}{
		map[string]any{
			"email":         order.Email,
			"status":        order.Status,
			"purchase_date": order.PurchaseDate.Unix(),
			"products":      order.Products,
		},
	})
	if err != nil {
		return fmt.Errorf("try to create order: %w", err)
	}

	if !resp.Tuples()[0][0].(bool) {
		return errs.ParseTarantoolError(resp.Tuples()[0][1].(map[any]any))
	}

	o, err := models.OrderFromTarantoolResp(resp.Tuples()[0][1].(map[any]any))
	if err != nil {
		return fmt.Errorf("try to create order: %w", err)
	}

	order.ID = o.ID
	order.UpdatedAt = o.UpdatedAt

	return nil
}

func (r *Repository) Delete(_ string) error {
	return nil
}

func (r *Repository) GetByID(id string) (*models.Order, error) {
	resp, err := r.conn.Select("orders", "pk", 0, 1, gotarantool.IterEq, []interface{}{id})
	if err != nil {
		return nil, fmt.Errorf("try to get order by id: %w", err)
	}

	if resp.Error != "" {
		return nil, fmt.Errorf("try to get order by id: %s", resp.Error)
	}

	logrus.Info(resp.Tuples())

	return nil, nil
}

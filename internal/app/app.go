package app

import (
	"errors"
	"net/http"
	"os"
	"syscall"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"

	"gitlab.com/insan1a/orders_service/internal/delivery/http/controller"
	"gitlab.com/insan1a/orders_service/internal/repository/orders"
	"gitlab.com/insan1a/orders_service/lib/gracefulgroup"
	"gitlab.com/insan1a/orders_service/lib/httpserver"
	"gitlab.com/insan1a/orders_service/lib/tarantool"
)

func Run() error {
	tarantoolCfg := tarantool.NewConfig("localhost:3301")

	tarantoolConn, err := tarantool.New(tarantoolCfg)
	if err != nil {
		return err
	}

	logrus.WithFields(logrus.Fields{
		"addr": "localhost:3301",
		"user": "guest",
	}).Info("Connected to Tarantool")

	repo := orders.New(tarantoolConn)

	router := mux.NewRouter()
	controller.MountEndpoints(router, repo)

	httpServerCfg := httpserver.NewConfig(":9929", router)
	httpServer := httpserver.New(httpServerCfg)

	gracefulGroupCfg := gracefulgroup.NewConfig().
		WithSignals(os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	group, ctx := gracefulgroup.New(gracefulGroupCfg)

	group.Go(func() error {
		logrus.WithFields(logrus.Fields{
			"addr": "127.0.0.1:9929",
		}).Info("Starting HTTP server")
		if err := httpServer.Start(); !errors.Is(err, http.ErrServerClosed) {
			return err
		}
		return nil
	})
	group.Go(func() error {
		<-ctx.Done()
		logrus.Info("Shutting down HTTP server gracefully")
		return httpServer.Stop(ctx)
	})
	group.Go(func() error {
		<-ctx.Done()
		logrus.Info("Shutting down Tarantool connection gracefully")
		return tarantoolConn.Close()
	})

	return group.Wait()
}

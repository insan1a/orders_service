package controller

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/insan1a/orders_service/internal/models"
)

type OrdersRepo interface {
	Create(order *models.Order) error
	GetByID(id string) (*models.Order, error)
	Delete(id string) error
}

type controller struct {
	repo OrdersRepo
}

func MountEndpoints(router *mux.Router, repo OrdersRepo) {
	c := &controller{
		repo,
	}

	router.HandleFunc("/orders", c.HandleCreateOrder).Methods(http.MethodPost)
	router.HandleFunc("/orders", c.HandleDeleteOrder).Methods(http.MethodDelete)
	router.HandleFunc("/orders/{order_id}", c.HandleGetOrderByID).Methods(http.MethodGet)
}

func (c *controller) HandleCreateOrder(w http.ResponseWriter, _ *http.Request) {
	var input models.Order

	if err := c.repo.Create(&input); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := json.NewEncoder(w).Encode(input); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *controller) HandleGetOrderByID(w http.ResponseWriter, _ *http.Request) {
	order, err := c.repo.GetByID("")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := json.NewEncoder(w).Encode(order); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *controller) HandleDeleteOrder(w http.ResponseWriter, _ *http.Request) {
	if err := c.repo.Delete(""); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := json.NewEncoder(w).Encode(map[string]any{}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

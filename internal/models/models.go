package models

import (
	"fmt"
	"reflect"
	"time"
)

type Product struct {
	Name   string  `msgpack:"name"`
	Amount int     `msgpack:"amount"`
	Price  float64 `msgpack:"price"`
}

type Order struct {
	ID           string    `msgpack:"id"`
	Email        string    `msgpack:"email"`
	Status       string    `msgpack:"status"`
	Products     []Product `msgpack:"products"`
	PurchaseDate time.Time `msgpack:"purchase_date"`
	UpdatedAt    time.Time `msgpack:"updated_at"`
}

func OrderFromTarantoolResp(data map[any]any) (*Order, error) {
	if err := validateRequiredOrderFields(data); err != nil {
		return nil, err
	}

	products, err := parseProducts(data["products"].(map[any]any))
	if err != nil {
		return nil, err
	}

	return &Order{
		ID:           data["id"].(string),
		Email:        data["email"].(string),
		Status:       data["status"].(string),
		Products:     products,
		PurchaseDate: time.Unix(int64(data["purchase_date"].(uint64)), 0),
		UpdatedAt:    time.Unix(int64(data["updated_at"].(uint64)), 0),
	}, nil
}

type field struct {
	name string
	typ  reflect.Type
}

func validateRequiredOrderFields(data map[any]any) error {
	var requiredFields = []field{
		{"id", reflect.TypeOf("")},
		{"email", reflect.TypeOf("")},
		{"status", reflect.TypeOf("")},
		{"products", reflect.TypeOf(map[any]any{})},
		{"purchase_date", reflect.TypeOf(uint64(0))},
		{"updated_at", reflect.TypeOf(uint64(0))},
	}

	for _, f := range requiredFields {
		val, ok := data[f.name]
		if !ok {
			return fmt.Errorf("missing field %s", f.name)
		}

		if valType := reflect.TypeOf(val); !valType.AssignableTo(f.typ) {
			return fmt.Errorf("invalid type for field %s: %s", f.name, valType)
		}
	}

	return nil
}

func parseProducts(data map[any]any) ([]Product, error) {
	var products []Product

	for _, v := range data {
		p, ok := v.(map[any]any)
		if !ok {
			return nil, fmt.Errorf("invalid product type - %#v", v)
		}

		if err := validateRequiredProductFields(p); err != nil {
			return nil, fmt.Errorf("invalid product fields: %s", err.Error())
		}

		if reflect.TypeOf(p["price"]).Kind() == reflect.Uint64 {
			p["price"] = float64(p["price"].(uint64))
		}

		products = append(products, Product{
			Name:   p["name"].(string),
			Amount: int(p["amount"].(uint64)),
			Price:  p["price"].(float64),
		})
	}

	return products, nil
}

func validateRequiredProductFields(data map[any]any) error {
	var requiredFields = []field{
		{"name", reflect.TypeOf("")},
		{"amount", reflect.TypeOf(uint64(0))},
		{"price", reflect.TypeOf(float64(0))},
	}

	for _, f := range requiredFields {
		val, ok := data[f.name]
		if !ok {
			return fmt.Errorf("missing field %s", f.name)
		}

		valType := reflect.TypeOf(val)

		if f.name == "price" {
			if valType.Kind() != reflect.Float64 && valType.Kind() != reflect.Uint64 {
				return fmt.Errorf("invalid type for field %s: %s", f.name, valType)
			}

			continue
		}

		if !valType.AssignableTo(f.typ) {
			return fmt.Errorf("invalid type for field %s: %s", f.name, valType)
		}
	}

	return nil
}

package main

import (
	"os"

	"github.com/sirupsen/logrus"

	"gitlab.com/insan1a/orders_service/internal/app"
)

func main() {
	setupLogger(os.Getenv("SERVICE_ENV"))

	if err := app.Run(); err != nil {
		logrus.WithError(err).Error("app run error")
	}
}

func setupLogger(env string) {
	switch env {
	case "production":
		logrus.SetLevel(logrus.InfoLevel)
		logrus.SetFormatter(new(logrus.JSONFormatter))
	case "development":
		logrus.SetLevel(logrus.DebugLevel)
		logrus.SetFormatter(new(logrus.JSONFormatter))
	case "local":
		logrus.SetLevel(logrus.DebugLevel)
		logrus.SetFormatter(new(logrus.TextFormatter))
	default:
		logrus.SetLevel(logrus.DebugLevel)
		logrus.SetFormatter(new(logrus.TextFormatter))
	}
}
